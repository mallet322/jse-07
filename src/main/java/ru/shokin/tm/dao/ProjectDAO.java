package ru.shokin.tm.dao;

import ru.shokin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectDAO {

    /**
     * Массив с созданными проектами
     */

    private List<Project> projects = new ArrayList<>();

    /**
     * Создание проекта
     */

    public Project create(String name) {
        final Project project = new Project(name);
        projects.add(new Project(name));
        return project;
    }

    /**
     * Удаление проектов
     */

    public void clear() {
        projects.clear();
    }

    /**
     * Список созданных проектов
     */

    public List<Project> findAll() {
        return projects;
    }

}