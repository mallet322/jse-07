package ru.shokin.tm.dao;

import ru.shokin.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskDAO {

    /**
     * Массив с созданными задачами
     */

    private List<Task> tasks = new ArrayList<>();

    /**
     * Создание задачи
     */

    public Task create(String name) {
        final Task task = new Task(name);
        tasks.add(new Task(name));
        return task;
    }

    /**
     * Удаление задачи
     */

    public void clear() {
        tasks.clear();
    }

    /**
     * Список созданных задач
     */

    public List<Task> findAll() {
        return tasks;
    }

}