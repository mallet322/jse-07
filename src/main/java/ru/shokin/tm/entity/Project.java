package ru.shokin.tm.entity;

public class Project {

    /**
     * Уникальный идентификатор проекта
     */

    private long id = System.nanoTime();

    /**
     * Наименование проекта
     */

    private String name = "";

    /**
     * Информация о проекте
     */

    private String description = "";

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Project id: " + id + ", Project name: " + name;
    }

}