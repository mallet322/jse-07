package ru.shokin.tm;

import ru.shokin.tm.dao.ProjectDAO;
import ru.shokin.tm.dao.TaskDAO;

import java.util.Scanner;

import static ru.shokin.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение для контроля статусов выполняемых задач
 */

public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    final static Scanner scanner = new Scanner(System.in);

    /**
     * Точка входа
     *
     * @param args параметры запуска
     */

    public static void main(final String[] args) {
        run(args);
        displayWelcome();
        process();
    }


    /**
     * Запуск приложения в режиме бесконечного цикла
     */

    private static void process() {
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    /**
     * Запуск приложения с аргументом из командной строки
     *
     * @param args - массив аргументов
     *             Для run используется перегрузка методов
     */

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;

        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * @param command - выполняемая команда
     * @return код ошибки или 0 в случае успешного завершения
     */

    private static int run(final String command) {
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return displayExit();

            case PROJECT_CREATE:
                return createProject();
            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_LIST:
                return listProject();

            case TASK_CREATE:
                return createTask();
            case TASK_CLEAR:
                return clearTask();
            case TASK_LIST:
                return listTask();

            default:
                return displayError();
        }
    }

    /**
     * Вывод на экран приветствия
     */

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Вывод на эркан информации о доступных командах
     *
     * @return 0
     */

    private static int displayHelp() {
        System.out.println("=== INFO PANEL ===");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Closing the application.");
        System.out.println();
        System.out.println("=== PROJECTS ===");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println("project-list  - Display list of projects.");
        System.out.println();
        System.out.println("=== TASKS ===");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        System.out.println("task-list  - Display list of tasks.");

        return 0;
    }

    /**
     * Вывод на экран версии приложения
     *
     * @return 0
     */

    private static int displayVersion() {
        System.out.println("1.0.3");
        return 0;
    }

    /**
     * Вывод на экран сведений о разработчике
     *
     * @return 0
     */

    private static int displayAbout() {
        System.out.println("Developer: Elias Shokin");
        System.out.println("Email: shokin_is@nlmk.com");
        return 0;
    }

    /**
     * Вывод на экран сообщения об ошибке
     *
     * @return -1
     */

    private static int displayError() {
        System.out.println("Error!!! Unknown program arguments...");
        return -1;
    }

    /**
     * Создание проекта
     *
     * @return 0
     */

    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        projectDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление проекта
     *
     * @return 0
     */

    private static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список проектов
     *
     * @return 0
     */

    private static int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println(projectDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Создание задания
     *
     * @return 0
     */

    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        taskDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление задания
     *
     * @return 0
     */

    private static int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список заданий
     *
     * @return 0
     */

    private static int listTask() {
        System.out.println("[LIST TASK]");
        System.out.println(taskDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Завершение работы приложения
     *
     * @return 0
     */

    private static int displayExit() {
        System.out.println("SEE YOU LATER! :)");
        System.exit(0);
        return 0;
    }

}