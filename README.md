 **Task manager**
=====================

### О проекте:
**Приложение для контроля статусов выполняемых задач.**

-----------------------------------
### Ссылка на альтернативный репозиторий в GitHub: [https://github.com/mallet322/jse](https://github.com/mallet322/jse)
### Требования к software:
* Java 11 или выше.
* Maven 3 или выше.
### Запуск приложения:
    cd <расположение проекта>/target/ 
    java -jar task-manager-<версия>.jar <аргументы запуска>
## Аргументы запуска приложения:
 `help`      Отображение терминальных команд.

 `version`   Отображение текущей версии приложения.

 `about`     Отображение информации о разработчике.

 -----------------------------------

 `project-create` Создание нового проекта.

 `project-clear` Удаление текущих проектов.

 `project-list` Список созданных проектов.

 -----------------------------------

 `task-create` Создание новой задачи.

 `task-clear` Удаление текущих задач.

 `task-list` Список созданных задач.
## Команды для сборки приложения

- Удаление всех созданных в процессе сборки артефактов:

```bash
mvn clean
```

- Создание .jar файла:

```bash
mvn package
```

- Сборка и копирование созданного файла в локальный репозиторий:

```bash
mvn install
```
## Разработчик: 
   ***Шокин Илья*** 

   mailto: [shokin_is@nlmk.com](mailto:shokin_is@nlmk.com)
